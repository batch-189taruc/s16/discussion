let firstNumber = 10
let secondNumber = 5
let total = 0
// addition operator
total = firstNumber + secondNumber
console.log('Result of addition operator is ' + total)

// subtraction operator
total = firstNumber - secondNumber
console.log('Result of subtraction operator is ' + total)
// multiplication operator
total = firstNumber * secondNumber
console.log('Result of multiplication operator is ' + total)
// division  operator
total = firstNumber / secondNumber
console.log('Result of division operator is ' + total)
// Modulo operator
total = firstNumber % secondNumber
console.log('Result of division operator is ' + total)

// ASSIGNMENTS OPERATORS - = only
total = 27
console.log(total)

total += 3
// total = total + 3
console.log(total)

total -= 5
console.log(total)

total *= 4
console.log(total)

total /= 20
console.log(total)

// Multiple operators MDAS rule
let mdasTotal = 0
mdasTotal = 2 + 1 - 5 * 4 / 1
console.log(mdasTotal)

let pemdasTotal = 0

// (**) is for exponential multiplication
pemdasTotal = 5**2 + (10 - 2) / 2 * 3
console.log(pemdasTotal)

// INCREMENT AND DECREMENT OPERATORS
// pre increment adds 1 first before reading the value
// pre decrement subtracts 1 first before reading the value
// post increment reads value first before adding 1
// post increment reads value first before subtracting 1

let incrementNumber = 1
let decrementNumber = 5
let resultofPreIncrement = ++incrementNumber
let resultOfPreDecrement = --decrementNumber
let resultofPostIncrement = incrementNumber++
let resultOfPostDecrement = decrementNumber--

console.log(resultofPreIncrement)
console.log(resultOfPreDecrement)
console.log(resultofPostIncrement)
console.log(resultOfPostDecrement)

// Coercion
let a = '10'
let b = 10

console.log(a + b)
// result is 1010 due to string+number

// non-coercion
let d = 10
let f = 10

console.log(d + f)
// to view the datatype, use typeof
let stringType = 'hello'
let numberType = 1
let booleanType = true
let arrayType = ['1', '2', '3']
let objectType ={
	objectKey: 'Object Value'
}

console.log(typeof stringType)
console.log(typeof numberType)
console.log(typeof booleanType)
console.log(typeof arrayType)
console.log(typeof objectType)

// true is being read as "1" in console, while false is "0"
console.log(true + 1)
console.log(false + 1)

// comparison command - values compared
console.log(5 == 5) 


// false because of wrong spelling
console.log('hello' == 'hellow')
// true because number in string is still number
console.log('2' == 2)


// false bec === is strict for datatype, compares BOTH value and datatype
console.log('2' === 2)

// != opposite of == (all false below)
console.log(1 != 1)
console.log('hello'!= 'hello')
console.log('2'!= 2)

// !== opposite of ===
console.log('1' !== 1)

// RELATIONAL OPERATORS
let firstVariable = 10
let secondVariable = 5
console.log(firstVariable > secondVariable)
console.log(firstVariable < secondVariable)

// statement is true because 10 is true to be greater or equal, even data types where compared
console.log(firstVariable >= secondVariable)

// LOGICAL OPERATORS
// for boolean comparison

// && - AND operator
let isLegalAge = true
let isRegistered = false
console.log(isLegalAge && isRegistered)

// || - OR operator
console.log(isLegalAge || isRegistered)

// ! NOT operator
console.log(!isLegalAge)

// the below will be true
console.log(isLegalAge && !isRegistered)

// Truthy and Falsey Values
// empty variables if compared to false (therefore if compared to false, it will become true), (null is special)

console.log([] == false)
console.log('' == false)
/*console.log(null == false)*/
console.log(0 == false)










